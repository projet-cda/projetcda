<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");

// Si l'utlisateur n'est pas loggé renvoie vers l'index
if (!isset($_SESSION['ID'])) {
    header("Location:index.php");
    die;
}

// Bannissemnt de l'utilisateur
if (isset($_GET['idUser'])) {
    $unUser = User::construct_load($_GET['idUser']);
    $unUser->ban($_GET['ban']);
}

if (isset($_GET['isAdmin'])) {
    header("Location: adminUser.php?modif=ok");
    die;
}

header("Location: panel.php");
die;
