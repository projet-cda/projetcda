<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
// Recupération infos sur l'utilisateur
$user = new User();
$user->loadUser($_SESSION['ID']);
// Si l'utilisateur est banni, renvoie vers panel
if ($user->isBanned) {
    header("Location:panel.php");
}

$annonce = new Annonce();
$erreur = null;

// Gestion du post
if (!empty($_POST)) {

    $idAnnonce = null;
    if (!empty($_POST['idAnnonce'])) {
        $idAnnonce = intval($_POST['idAnnonce']);
    }

    $titre = null;
    if (!empty($_POST['titre'])) {
        $titre = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['titre']))))));
    }

    $prix = null;
    if (!empty($_POST['prix'])) {
        $prix = floatval($_POST['prix']);
    }

    $description = null;
    if (!empty($_POST['description'])) {
        $description = str_replace(array("<script>,</script>"), array("", ""), $_POST['description']);
    }

    if ($titre && $description && $prix) {
        $annonce->id = $idAnnonce;
        $annonce->titre = $titre;
        $annonce->prix = $prix;
        $annonce->description = $description;
        $annonce->save();

        if (!empty($_FILES["fileToUpload"]['name'])) {
            $target_dir = "img/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

            $_FILES["fileToUpload"]["name"] = $annonce->id . "." . $imageFileType;
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

            move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);

            $annonce->photo = new Photo();
            $annonce->photo->url = $target_file;

            $annonce->photo->upload($annonce->id);
        }

        header("Location:panel.php?modif=ok");
    } else {
        $erreur = "Echec de la sauvegarde";
    }
}

// Recupération de l'annonce en mode edition
if (isset($_GET['idAnnonce'])) {
    $annonce->load(intval($_GET['idAnnonce']));
}
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <script src="lib/js/jquery-3.5.1.min.js"></script>
    <script src="lib/js/jquery.validate.js" referrerpolicy="origin"></script>
    <script src="lib/js/tinymce/tinymce.min.js"></script>
    <script>
        $(function() {
            tinymce.init({
                selector: 'textarea',
                invalid_elements: 'script',
                width: 600,
                height: 400,
            });
        });
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableau de bord - Ajout/Modification d'une annonce</title>
</head>

<body>
    <?php
    require('header.php');
    if ($user->isBanned) {
    ?>
        <h2>Compte désactivé, veuillez contacter l'administrateur du site</h2>
    <?php
    } else {
    ?>
        <div class="container" style="margin: 100px auto;">
            <h1 class="display-4">Tableau de bord "<?= $user->nom ?> <?= $user->prenom ?>"</h1>
            <div class="jumbotron" style="background-color:#F19820">
                <ul class="nav nav-pills nav-fill nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="panel.php">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="panel_stat.php">Mes stats</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="panel_infos.php">Mes infos</a>
                    </li>
                </ul>
                <div class="container jumbotron" style="width: 650px;margin-top:50px;background-color:#F19820">
                    <h2>Ajout/ modification d'une annonce</h2>
                    <?php
                    if ($erreur) {
                        echo "<div class='alert alert-danger'>$erreur</div>";
                    }
                    ?>
                    <form action="#" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="titre">Titre : </label>
                            <input class="form-control" type="text" name="titre" value="<?= $annonce->titre ?>" required maxlength="22"><br>
                            <label for="prix">Prix : </label>
                            <input class="form-control" type="number" step="0.01" min="0" name="prix" value="<?= $annonce->prix ?>" required><br>
                            <label for="description">Description</label><br>
                            <textarea name="description"><?= $annonce->description ?></textarea>
                            <!-- Module photo -->
                            <label for="fileToUpload">Image : </label><br>
                            <input class="btn-primary" type="file" name="fileToUpload" id="fileToUpload"><br><br>
                            <input class="btn-primary" type="submit" value="Valider">
                            <button class="btn-primary" onclick="window.location='panel.php'">Retour</button>
                            <input type="hidden" name="idAnnonce" value="<?= $annonce->id ?>">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>
</body>

</html>