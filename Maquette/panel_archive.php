<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
$user = new User();
$user->loadUser($_SESSION['ID']);
$user->getIdPaniers();
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableau de bord</title>
</head>

<body>
    <?php
    require('header.php');
    if ($user->isBanned) {
    ?>
        <h2>Compte désactivé, veuillez contacter l'administrateur du site</h2>
    <?php
    } else {
    ?>
        <div class="container" style="margin: 100px auto;">
            <h1 class="display-4">Tableau de bord "<?=$user->nom?> <?=$user->prenom?>"</h1>
            <div class="jumbotron" style="background-color:#F19820">
                <ul class="nav nav-pills nav-fill nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="panel.php">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="panel_stat.php">Mes stats</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="panel_archive.php">Mes commandes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="panel_infos.php">Mes infos</a>
                    </li>
                </ul>
                <h1>Mes commandes</h1>
                <div>
                    <?php
                    for ($i = 0; $i < count($user->idPaniersArchives); $i++) {
                        $panier = new Panier();
                        $panier->id = $user->idPaniersArchives[$i];
                        $panier->getArchives();
                        $sous_total = 0;
                        $reductions = 0;
                        $frais_livraison = 6.95;
                        $montant_total = 0;
                    ?><div class="container" style="background-color: white;">
                            <div class="alert alert-light">
                                <h4> Commande passée le <?= $panier->dateValidation->format("d/m/Y à H:i") ?> </h4>
                                <p>Vos articles</p>
                                <?php for ($j = 0; $j < count($panier->annoncesArchives); $j++) {
                                    $sous_total += $panier->annoncesArchives[$j]->prix;
                                ?>
                                    <div class="alert alert-warning">
                                        <h2><?= $panier->annoncesArchives[$j]->titre ?></h2>
                                        <p><?= nl2br($panier->annoncesArchives[$j]->description) ?></p>
                                        <p><?= $panier->annoncesArchives[$j]->prix ?> €</p>
                                    </div>
                                <?php
                                }
                                $montant_total += $sous_total;
                                ?>
                                <div class="alert alert-primary">
                                    <h4>Récapitulatif de votre commande</h4>
                                    Sous-total de vos achat : <?= number_format($sous_total, 2) ?>€<br>
                                    Reductions : <?= number_format($reductions, 2) ?>€<br>
                                    Livraison : <?= number_format($frais_livraison, 2) ?>€<br>
                                    <p>Montant Total <?= number_format($montant_total, 2) ?>€</p>
                                </div>

                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>

</body>

</html>