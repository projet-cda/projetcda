<?php
session_start();

// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Recupération des annnonces
if (!isset($_GET['idAnnonce'])) {
    header("Location:index.php");
}
$idAnnonce = intval($_GET['idAnnonce']);

$annonce = new Annonce();
$annonce->load($idAnnonce);

if (isset($_SESSION['ID'])) {
    if ($_SESSION['isAdmin']) {
        header("Location:admin.php");
    } else {
        $panier = new Panier();
        $panier->verifExistPanier();
    }
}
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <link rel="stylesheet" href="lib/css/styles3.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail annonce</title>
</head>

<body>
    <?php
    require('header.php');
    ?>
    <div class="container" style="margin: 100px auto;">
        <div class="jumbotron" style="background-color:#F19820">
            <h1 class="display-4">Detail annonce</h1><br>
            <div class="card float-left bg-light" style="width: 90%; margin:30px;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4"> <?php if (isset($annonce->photo->id)) { ?>
                                <img src="<?= $annonce->photo->url ?>" class="card-img-top" alt="Image annonce <?= $annonce->id ?>">
                            <?php } else { ?>
                                <img src="img/defaut.jpg" class="card-img-top" alt="image par defaut">
                            <?php } ?>
                        </div>
                        <div class="col-sm-8">
                            <div class="card-text" style="width: 100%; margin:30px;">
                                <h5><?= $annonce->titre ?></h5>
                                <p>Publiée le <?= $annonce->date->format("d/m/Y à H:i") ?></p>
                                <div><?= $annonce->description ?></div>
                                Prix : <?= $annonce->prix ?> €<br>
                                <?php if (isset($_SESSION['ID'])) {
                                    if ($annonce->idClient !== $_SESSION['ID']) {
                                        if ($panier->verifExistDansPanier($annonce->id)) { ?>
                                            <a class="btn btn-primary">Déjà dans votre panier</a>
                                        <?php } else { ?>
                                            <a href="ajoutPanier.php?idAnnonce=<?= $annonce->id ?>" class="btn btn-primary">Ajouter au panier</a>
                                        <?php }
                                    } else { ?>
                                        <a class="btn btn-primary">Ceci est votre annonce</a>
                                    <?php
                                    }
                                } else { ?>
                                    <a href="login.php" class="btn btn-primary">Connectez-vous</a>
                                <?php }

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>