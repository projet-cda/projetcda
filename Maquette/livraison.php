<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
// Infos user
$user = new User();
$user->loadUser($_SESSION['ID']);
// Infos panier
$panier = new Panier();
$panier->idClient = $_SESSION['ID'];
$panier->getAnnonces();
// Initialisation des variables pour le recapitulatif
$sous_total = 0;
$reductions = 0;
$frais_livraison = 6.95;
$montant_total = 0;

// Traitement du post
$erreur = null;
if (!empty($_POST)) {
    $tel = null;
    if (!empty($_POST['tel'])) {
        $user->telephone = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['tel']))))));
    }
    $cp = null;
    if (!empty($_POST['cp'])) {
        $user->cp = intval($_POST['cp']);
    }
    $rue = null;
    if (!empty($_POST['rue'])) {
        $user->rue = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['rue']))))));
    }
    $ville = null;
    if (!empty($_POST['ville'])) {
        $user->ville = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['ville']))))));
    }
    $user->id = $_SESSION['ID'];
    $user->save();
    header("Location:livraison.php?modif=ok");
}
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <script src="lib/js/jquery-3.5.1.min.js"></script>
    <script src="lib/js/jquery.validate.js" referrerpolicy="origin"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Livraison</title>
</head>

<body>
    <?php
    require('header.php');
    ?><br>
    <div class="container" style="margin: 100px auto;">
        <div class="jumbotron" style="background-color:#F19820">
            <ul class="nav nav-pills nav-fill nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="panier.php">Panier</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active">Livraison</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Paiement</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Confirmation</a>
                </li>
            </ul>

            <!-- Formulaire -->
            <div class="container">
                <h1>Livraison</h1>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="alert alert-warning">
                            <?php
                            if (!empty($_GET['modif'])) {
                                if ($_GET['modif'] === "ok") {
                                    echo "<div class='alert alert-danger'>Mise à jour effectuée</div>";
                                } else {
                                    echo "<div class='alert alert-danger'>Veuillez remplir tous les champs</div>";
                                }
                            } ?>
                            <form action="#" method="POST">
                                <div class="form-group">
                                    <label for="nom">Nom </label>
                                    <input class="form-control" type="text" name="nom" placeholder="nom" value="<?= $user->nom ?>" disabled><br>
                                    <label for="rue">Prenom</label>
                                    <input class="form-control" type="text" name="prenom" placeholder="prenom" value="<?= $user->prenom ?>" disabled><br>
                                    <label for="tel">Telephone</label>
                                    <input class="form-control" type="text" name="tel" placeholder="telephone" value="<?= $user->telephone ?>" required><br>
                                    <label for="rue">Rue</label>
                                    <input class="form-control" type="text" name="rue" placeholder="rue" value="<?= $user->rue ?>" required><br>
                                    <label for="cp">Code postal</label>
                                    <input class="form-control" type="number" name="cp" placeholder="code postal" value="<?= $user->cp ?>" required><br>
                                    <label for="ville">Ville</label>
                                    <input class="form-control" type="text" name="ville" placeholder="ville" value="<?= $user->ville ?>" required><br>
                                    <input class="btn-secondary" style="margin-bottom: 10px;" type="submit" value="Valider adresse">
                                </div>
                            </form>
                        </div>
                        <form action="paiement.php">
                            <input class="btn-success" type="submit" value="Procéder au paiement">
                        </form>
                    </div>
                    <!-- Mon panier -->
                    <div class="col-sm-4">
                        <div class="alert alert-primary">
                            <h4>Mon panier</h4>
                            <?php
                            for ($i = 0; $i < count($panier->annonces); $i++) {
                                $sous_total += $panier->annonces[$i]->prix;
                            ?><div style="border: 1px solid #F19820;padding:10px;margin:10px">
                                    <h2><?= $panier->annonces[$i]->titre ?></h2>
                                    <p><?= $panier->annonces[$i]->description ?></p>
                                    <p><?= $panier->annonces[$i]->prix ?> €</p>
                                </div><?php }
                                    $montant_total = $sous_total - $reductions + $frais_livraison;
                                        ?>
                            <!-- Recapitulatif -->
                        </div>
                        <div class="alert alert-primary">
                            <h4>Récapitulatif de votre commande</h4>
                            Sous-total de vos achat : <?= number_format($sous_total, 2) ?>€<br>
                            Reductions : <?= number_format($reductions, 2) ?>€<br>
                            Livraison : <?= number_format($frais_livraison, 2) ?>€<br>
                            <p>Montant Total <?= number_format($montant_total, 2) ?>€</p>
                        </div>
                        <!-- Fin recapitulatif -->
                    </div>
                </div>
                <!-- Fin panier -->
            </div>
        </div>
    </div>
</body>

</html>