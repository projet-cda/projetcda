<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
// Recuperation infos utilisateur
$user = new User();
$user->loadUser($_SESSION['ID']);
if (!$user->rue || !$user->cp || !$user->ville || !$user->telephone) {
    header("Location:livraison.php?modif=ko");
}
// Infos panier
$panier = new Panier();
$panier->idClient = $_SESSION['ID'];
$panier->getAnnonces();
// Initialisation des variables pour le recapitulatif
$sous_total = 0;
$reductions = 0;
$frais_livraison = 6.95;
$montant_total = 0;


// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paiement</title>
</head>

<body>
    <?php
    require('header.php');
    ?><br>
    <div class="container" style="margin: 100px auto;">
        <div class="jumbotron" style="background-color:#F19820">
            <ul class="nav nav-pills nav-fill nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="panier.php">Panier</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="livraison.php">Livraison</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active">Paiement</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Confirmation</a>
                </li>
            </ul>

            <!-- Paiement -->
            <div class="container">
                <h1>Paiement</h1>
                <div class="row">
                    <div class="col-sm-8">
                        <form method="POST" action="success.php">
                            <div class="alert alert-warning">
                                <p>Réference paiement : 400224208500</p>
                                <p>Mode de paiement : Visa</p>
                                <p>Montant : EUR 32.95</p>
                                <p>Numero de la carte <input type="text"></p>
                                <p>Date d'expiration
                                    <select>
                                        <option>--</option>
                                        <?php for ($i = 0; $i <= 31; $i++) {
                                            echo "<option>$i</option>";
                                        } ?>
                                    </select>
                                    <select>
                                        <option>----</option>
                                        <?php for ($i = 2020; $i <= 2022; $i++) {
                                            echo "<option>$i</option>";
                                        } ?>
                                    </select></p>
                                <p>Titulaire de la carte <input type="text"></p>
                                <p>Code de sécurité <input type="text"></p>
                            </div>
                            <input class="btn-success" type="submit" value="Effectuer le paiement">
                        </form>
                    </div><!-- Mon panier -->
                    <div class="col-sm-4">
                        <div class="alert alert-primary">
                            <h4>Mon panier</h4>
                            <?php
                            for ($i = 0; $i < count($panier->annonces); $i++) {
                                $sous_total += $panier->annonces[$i]->prix;
                            ?><div style="border: 1px solid #F19820;padding:10px;margin:10px">
                                    <h2><?= $panier->annonces[$i]->titre ?></h2>
                                    <p><?= $panier->annonces[$i]->description ?></p>
                                    <p><?= $panier->annonces[$i]->prix ?> €</p>
                                </div><?php }
                                    $montant_total = $sous_total - $reductions + $frais_livraison;
                                        ?>
                            <!-- Recapitulatif -->
                        </div>
                        <div class="alert alert-primary">
                            <h4>Récapitulatif de votre commande</h4>
                            Sous-total de vos achat : <?= number_format($sous_total, 2) ?>€<br>
                            Reductions : <?= number_format($reductions, 2) ?>€<br>
                            Livraison : <?= number_format($frais_livraison, 2) ?>€<br>
                            <p>Montant Total <?= number_format($montant_total, 2) ?>€</p>
                        </div>
                        <!-- Fin recapitulatif -->
                    </div>
                </div>
                <!-- Fin panier -->
            </div>
        </div>
</body>

</html>