<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
$user = new User();
$erreur = null;
// Traitement du POST
if (!empty($_POST)) {
    $nom = null;
    if (!empty($_POST['nom'])) {
        $nom = strtoupper(addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['nom'])))))));
    }
    $prenom = null;
    if (!empty($_POST['prenom'])) {
        $prenom = ucfirst(strtolower(addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['prenom']))))))));
    }
    $tel = null;
    if (!empty($_POST['tel'])) {
        $tel = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['tel']))))));
    }
    if (!$nom || !$prenom) {
        $erreur = "Veuillez renseigner tous les champs";
    } else {
        $user->id = $_SESSION['ID'];
        $user->nom = $nom;
        $user->prenom = $prenom;
        $user->telephone = $tel;
        $user->majCoordonnees();
        header("Location:panel_infos.php?modif=ok");
    }
}
$user->loadUser($_SESSION['ID']);
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableau de bord</title>
</head>

<body>
    <?php
    require('header.php');
    if ($user->isBanned) {
    ?>
        <h2>Compte désactivé, veuillez contacter l'administrateur du site</h2>
    <?php
    } else {
    ?>
        <div class="container" style="margin: 100px auto;">
            <h1 class="display-4">Tableau de bord "<?= $user->nom ?> <?= $user->prenom ?>"</h1>
            <div class="jumbotron" style="background-color:#F19820">
                <ul class="nav nav-pills nav-fill nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="panel.php">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="panel_stat.php">Mes stats</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="panel_archive.php">Mes commandes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="panel_infos.php">Mes infos</a>
                    </li>
                </ul>
                <div class="container" style="margin-top: 50px;">
                    <div class="row">
                        <div class="col-4">
                            <ul class="nav flex-column  nav-pills nav-fill nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" href="panel_infos.php">Coordonnées</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="panel_adress.php">Adresse</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="panel_email.php">Email</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="panel_pwd.php">Mot de passe</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-8">
                            <div class="container jumbotron" style="width: 90%;">
                                <?php
                                if ($erreur) {
                                    echo "<div class='alert alert-danger'>$erreur</div>";
                                }
                                if (!empty($_GET['modif'])) {
                                    echo "<div class='alert alert-danger'>Mise à jour effectuée</div>";
                                }
                                ?><form action="#" method="POST">
                                    <div class="form-group">
                                        <label for="nom">Nom : </label>
                                        <input class="form-control" type="text" name="nom" placeholder="nom" value="<?= $user->nom ?>" required><br>
                                        <label for="prenom">Prenom : </label>
                                        <input class="form-control" type="text" name="prenom" placeholder="prenom" value="<?= $user->prenom ?>" required><br>
                                        <label for="tel">Téléphone : </label>
                                        <input class="form-control" type="text" name="tel" placeholder="telephone" value="<?= $user->telephone ?>" required><br>
                                        <input class="btn btn-primary" type="submit" value="Mettre à jour">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <?php } ?>
        </div>
</body>

</html>