<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}

// Suppression de l'annonce du panier
if(isset($_GET['idAnnonce'])&&(isset($_GET['idPanier']))){
    $panier = new Panier();
    $panier->deleteAnnoncePanier(intval($_GET['idAnnonce']),intval($_GET['idPanier']));
}

header("Location: panier.php");
die;