<?php

class Photo extends Model
{
    public $id;
    public $url;

    function __construct()
    {
        parent::__construct();
    }

    public function upload($idAnnonce)
    {
        $photo = Photo::construct_load($idAnnonce);
        if (isset($photo->id)) {
            $photo->deletePhoto();
        }

        $req = "INSERT INTO photo (url, idAnnonce) VALUES (:url, :idAnnonce)";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":url"        => $this->url,
            ":idAnnonce"  => $idAnnonce
        ]);

        $this->id = $this->pdo->lastInsertId();
    }

    public static function construct_load($id)
    {
        $photo = new Photo();
        $photo->load($id);
        return $photo;
    }

    /*
    * Charges les infos de l'utilisateur
    */
    public function load($id)
    {

        $req = "SELECT * FROM photo WHERE idAnnonce=:idAnnonce LIMIT 1";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute(array(":idAnnonce" => $id));
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
    }

    public function deletePhoto()
    {
        $req = "DELETE FROM photo WHERE id = :id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute(array(":id" => $this->id));

        unlink($this->url);
    }
}
