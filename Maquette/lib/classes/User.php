<?php

require_once "Annonce.php";

class User extends Model
{

    protected $id;
    protected $nom;
    protected $prenom;
    protected $rue;
    protected $cp;
    protected $ville;
    protected $email;
    protected $telephone;
    protected $pwd;
    protected $isAdmin;
    protected $isBanned;
    protected $lesAnnonces;
    protected $idPaniersArchives;
    public $infos;

    function __construct()
    {
        parent::__construct();
        $this->lesAnnonces = array();
    }

    public static function construct_load($id)
    {
        $user = new User();
        $user->loadUser($id);
        return $user;
    }

    /*
    * Charge les infos de l'utilisateur
    */
    function loadUser($id)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM user WHERE id = :id");
        $stmt->execute(['id' => $id]);
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
    }

    /*
    * Verifie si il existe déjà un utilisateur qui utilise cet email
    */
    function existEmail(): bool
    {
        $stmt = $this->pdo->prepare("SELECT * FROM user WHERE email=:email");
        $stmt->execute([
            ":email" => $this->email
        ]);
        if ($stmt->RowCount()) {
            return true;
        }
        return false;
    }

    /*
    * Verifie si les infos entrées lors du login, sont bonnes
    */
    function existUser(): bool
    {
        $stmt = $this->pdo->prepare("SELECT id, pwd FROM user WHERE email=:email");
        $stmt->execute([
            ":email" => $this->email
        ]);
        if ($stmt->RowCount()) {
            $tab = $stmt->fetch();
            if (password_verify($this->pwd, $tab['pwd'])) {
                $this->id = $tab['id'];
                return true;
            }
        }
        return false;
    }

    /*
    * Ajout d'un utilisateur
    */
    function register(): void
    {
        $req = "INSERT INTO user (nom, prenom, email, pwd) VALUES (:nom, :prenom, :email, :pwd)";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":nom"      => $this->nom,
            ":prenom"   => $this->prenom,
            ":email"    => $this->email,
            ":pwd"      => $this->pwd
        ]);
        $this->id = $this->pdo->lastInsertId();
    }

    /*
    * Sauvegarde générale infos utilisateur
    */
    function save()
    {
        $req = "UPDATE user SET nom=:nom, prenom=:prenom, telephone=:tel, rue=:rue, cp=:cp, ville=:ville WHERE id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":nom"      => $this->nom,
            ":prenom"   => $this->prenom,
            ":tel"      => $this->telephone,
            ":rue"      => $this->rue,
            ":cp"       => $this->cp,
            ":ville"    => $this->ville,
            ":id"       => $this->id
        ]);
    }

    /*
    * Sauvegarde coordonnées
    */
    function majCoordonnees()
    {
        $req = "UPDATE user SET nom=:nom, prenom=:prenom, telephone=:telephone WHERE id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":nom"          => $this->nom,
            ":telephone"    => $this->telephone,
            ":prenom"       => $this->prenom,
            ":id"           => $this->id
        ]);
    }

    /*
    * Sauvegarde adresse
    */
    function majAdress()
    {
        $req = "UPDATE user SET rue=:rue, cp=:cp, ville=:ville WHERE id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":rue"      => $this->rue,
            ":cp"       => $this->cp,
            ":ville"    => $this->ville,
            ":id"       => $this->id
        ]);
    }

    /*
    * Sauvegarde mot de passe
    */
    function majPwd()
    {
        $req = "UPDATE user SET pwd=:pwd WHERE id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":pwd"      => $this->pwd,
            ":id"       => $this->id
        ]);
    }

    /*
    * Sauvegarde email
    */
    function majEmail()
    {
        $req = "SELECT * FROM user WHERE email=:email AND id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":email"        => $this->email,
            ":id"           => $this->id
        ]);
        if ($stmt->RowCount()) {
            return "Pas de modification effectuée";
        } else {
            $req = "SELECT * FROM user WHERE email=:email AND id<>:id";
            $stmt = $this->pdo->prepare($req);
            $stmt->execute([
                ":email"        => $this->email,
                ":id"           => $this->id
            ]);
            if ($stmt->RowCount()) {
                return "Cet email est déjà utilisée";
            } else {
                $req = "UPDATE user SET email=:email WHERE id=:id";
                $stmt = $this->pdo->prepare($req);
                $stmt->execute([
                    ":email"        => $this->email,
                    ":id"           => $this->id
                ]);
                return "Mise à jour effectuée";
            }
        }
    }

    /*
    * Verif si le mon de passe est correct (Panel -> Infos)
    */
    function verifPwd()
    {
        $stmt = $this->pdo->prepare("SELECT id, pwd FROM user WHERE id=:id");
        $stmt->execute([
            ":id" => $this->id
        ]);
        if ($stmt->RowCount()) {
            $tab = $stmt->fetch();
            if (password_verify($this->pwd, $tab['pwd'])) {
                return true;
            }
        }
        return false;
    }

    function __get($name)
    {
        return $this->$name;
    }

    function __set($name, $value)
    {
        $this->$name = $value;
    }

    /*
    * Renvoie toutes les annonces d'un utilisateur , suivant leur statut
    */
    function getAnnonces($value = null)
    {
        $resultats = array();
        if ($value === 1) {
            $sql = "AND isPublie=0";
        } elseif ($value === 2) {
            $sql = "AND isPublie=1 and isAchete=0";
        } elseif ($value === 3) {
            $sql = "AND isAchete=1";
        } else {
            $sql = "";
        }
        $req = "SELECT id FROM annonce WHERE idClient = :idClient $sql ORDER BY date DESC";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([':idClient' => $this->id]);
        $resultats = $stmt->fetchAll(PDO::FETCH_ASSOC);

        for ($i = 0; $i < count($resultats); $i++) {
            $this->lesAnnonces[] = Annonce::construct_load($resultats[$i]['id']);
        }

        $this->getInfos();
    }

    /*
    * Renvoie les infos sur les annonces d'un utilisateur
    */
    function getInfos()
    {
        $req = "SELECT
        (SELECT count(*) from annonce where isPublie=1 AND isAchete=0 AND idClient=:id) AS en_cours,
        (SELECT count(*) from annonce where isAchete=1 AND idClient=:id AND DATEDIFF(now(),date)<30) AS terminee,
        (SELECT sum(prix) from annonce where idClient=:id AND isAchete=1) AS CA";

        $stmt = $this->pdo->prepare($req);
        $stmt->execute([":id" => $_SESSION['ID']]);
        $this->infos = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($this->infos['CA'] == null) {
            $this->infos['CA'] = 0;
        }
    }

    public function ban($isBanned)
    {
        $this->isBanned = $isBanned;

        $req = "UPDATE user SET isBanned = :isBanned WHERE id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":id"              => $this->id,
            ":isBanned"        => $this->isBanned
        ]);
    }

    function getIdPaniers()
    {
        $req = "SELECT id FROM panier WHERE isValide=1 AND idClient=:idClient ORDER BY dateValidation DESC";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([":idClient" => $_SESSION['ID']]);
        $resultats = $stmt->fetchAll(PDO::FETCH_ASSOC);
        for ($i = 0; $i < count($resultats); $i++) {
            $this->idPaniersArchives[] = $resultats[$i]['id'];
        }
    }
}
