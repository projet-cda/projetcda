<?php

class Model
{

    protected $pdo;
    protected $annonces;
    protected $users;

    public function __construct()
    {
        $this->pdo = Database::getPdo();
    }

    /*
    * Retourne toutes les annonces
    */
    public function getAllAnnonces($param=null)
    {
        $sql = "WHERE isPublie=1 AND isAchete=0";
        if($param===2){$sql="WHERE isPublie=0 ";}
        elseif($param===3){$sql="WHERE isAchete=1";}
        $resultats = array();
        $requete = "SELECT id FROM annonce $sql ORDER BY date DESC";
        $stmt = $this->pdo->prepare($requete);
        $stmt->execute();
        $resultats = $stmt->fetchAll();

        for ($i = 0; $i < count($resultats); $i++) {
            $this->annonces[] = Annonce::construct_load($resultats[$i]['id']);
        }
    }

    /*
    * Retourne toutes les annonces publiées et non achetées
    */
    public function getAnnoncesEnCours()
    {
        $resultats = array();
        $requete = "SELECT id FROM annonce WHERE isPublie=1 AND isAchete=0 ORDER BY date DESC";
        $stmt = $this->pdo->prepare($requete);
        $stmt->execute();
        $resultats = $stmt->fetchAll();

        for ($i = 0; $i < count($resultats); $i++) {
            $this->annonces[] = Annonce::construct_load($resultats[$i]['id']);
        }
    }

    public function getAllUsers()
    {
        $resultats = array();
        $requete = "SELECT id FROM user WHERE isAdmin=0";
        $stmt = $this->pdo->prepare($requete);
        $stmt->execute();
        $resultats = $stmt->fetchAll();

        for ($i = 0; $i < count($resultats); $i++) {
            $this->users[] = User::construct_load($resultats[$i]['id']);
        }
    }



    // GETTER
    function __get($name)
    {
        return $this->$name;
    }
}
