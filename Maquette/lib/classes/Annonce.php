<?php

class Annonce extends Model
{

    public $id;
    public $titre;
    public $description;
    public $date;
    public $prix;
    public $isPublie;
    public $isAchete;
    public $photo;

    function __construct()
    {
        parent::__construct();
    }

    public static function construct_load($id)
    {
        $annonce = new Annonce();
        $annonce->load($id);
        return $annonce;
    }

    /*
    * Charges les infos de l'utilisateur
    */
    public function load($id)
    {

        $req = "SELECT * FROM annonce WHERE id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute(array(":id" => $id));
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();

        $this->setDate();
        $this->photo = Photo::construct_load($id);
    }

    // SETTER $date
    public function setDate()
    {
        $this->date = new DateTime($this->date);
    }

    /*
    * Suppression d'une annonce
    */
    public function deleteAnnonce()
    {
        $req = "DELETE FROM annonce WHERE id = :id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute(array(":id" => $this->id));
    }

    /*
    * Ajout / modification d'une annonce
    */
    public function save()
    {
        if ($this->id) {
            $req = "UPDATE annonce SET titre=:titre, description=:description, prix=:prix WHERE id=:id";
            $stmt = $this->pdo->prepare($req);
            $stmt->execute([
                ":id"           => $this->id,
                ":titre"        => $this->titre,
                ":description"  => $this->description,
                ":prix"         => $this->prix
            ]);
        } else {
            $req = "INSERT INTO annonce (titre, description, prix, idClient) VALUES (:titre, :description, :prix, :idClient)";
            $stmt = $this->pdo->prepare($req);
            $stmt->execute([
                ":titre"        => $this->titre,
                ":description"  => $this->description,
                ":prix"         => $this->prix,
                ":idClient"     => $_SESSION['ID']
            ]);
            $this->id = $this->pdo->lastInsertId();
        }
    }

    public function validate()
    {
        $this->isPublie = 1;
        
        $req = "UPDATE annonce SET isPublie = :isPublie WHERE id=:id";
        $stmt = $this->pdo->prepare($req);
        $stmt->execute([
            ":id"              => $this->id,
            ":isPublie"        => $this->isPublie
        ]);
    }
}
