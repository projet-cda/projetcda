<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si lu'utlisateur est déjà loggé, renvoie vers l'index
if (isset($_SESSION['ID'])) {
    header("Location:index.php");
}
// Traiement du post
$erreur = null;
$user = new User();
if (!empty($_POST)) {
    $nom = null;
    if (!empty($_POST['nom'])) {
        $user->nom = strtoupper(addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['nom'])))))));
    }
    $prenom = null;
    if (!empty($_POST['prenom'])) {
        $user->prenom = ucfirst(strtolower(addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['prenom']))))))));
    }
    $email = null;
    if (!empty($_POST['email'])) {
        $user->email = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['email']))))));
    }
    $pwd = null;
    if (!empty($_POST['pwd'])) {
        $user->pwd = password_hash($_POST['pwd'], PASSWORD_DEFAULT);
    }
    if (($_POST['pwd'] != $_POST['pwd2']) || empty($_POST['pwd'])) {
        $erreur = "Mots de passe différents";
    } elseif ($user->existEmail()) {
        $erreur = "Email déjà utilisé";
    } else {
        $user->register();
        $user->loadUser($user->id);
        $_SESSION['ID'] = $user->id;
        $_SESSION['isAdmin'] = $user->isAdmin;
        if ($_SESSION['isAdmin']) {
            header("Location:admin.php");
        } else {
            header("Location:panel.php");
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <script src="lib/js/jquery-3.5.1.min.js"></script>
    <script src="lib/js/jquery.validate.js" referrerpolicy="origin"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
</head>

<body>
    <?php
    require('header.php');
    ?>
    <br>
    <div class="container jumbotron" style="width: 700px; background-color:#F19820">
        <h1 class="display-4">Inscription</h1>
        <?php
        if ($erreur) {
            echo "<div class='alert alert-danger'>$erreur</div>";
        }
        ?>
        <form action="#" method="POST">
            <div class="form-group">
                <label for="nom">Nom : </label>
                <input class="form-control" type="text" name="nom" placeholder="nom" value="<?= $user->nom ?>" required><br>
                <label for="prenom">Prenom : </label>
                <input class="form-control" type="text" name="prenom" placeholder="prenom" value="<?= $user->prenom ?>" required><br>
                <label for="email">Email : </label>
                <input class="form-control" type="email" name="email" placeholder="email" value="<?= $user->email ?>" required><br>
                <label for="pwd">Mot de passe : </label>
                <input class="form-control" type="password" name="pwd" required><br>
                <label for="pwd2">Repeter : </label>
                <input class="form-control" type="password" name="pwd2" required><br>
                <input class="btn btn-primary" type="submit" value="S'inscrire">
            </div>
        </form>
    </div>

</body>

</html>