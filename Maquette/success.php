<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
// Infos user
$panier = new Panier();
$panier->idClient = $_SESSION['ID'];
$panier->getAnnonces();
// Infos panier
$panier = new Panier();
$panier->idClient = $_SESSION['ID'];
$panier->getAnnonces();
// Initialisation des variables pour le recapitulatif
$sous_total = 0;
$reductions = 0;
$frais_livraison = 6.95;
$montant_total = 0;

// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirmation</title>
</head>

<body>
    <?php
    require('header.php');
    ?><br>
    <div class="container" style="margin: 100px auto;">
        <div class="jumbotron" style="background-color:#F19820">
            <ul class="nav nav-pills nav-fill nav-tabs">
                <li class="nav-item">
                    <a class="nav-link">Panier</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Livraison</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Paiement</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active">Confirmation</a>
                </li>
            </ul>


            <div class="alert alert-success" style="margin: 20px;">
                <h1>Nous vous remercions ...</h1>
                <p>De votre commande et nous félicitons de voir que cet article vous a echanté.</p>
                <p> Votre commande est en cours de traitement et vous sera envoyée très bientôt.</p>
            </div> <!-- Mon panier -->
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <div class="alert alert-primary">
                            <h4>Mon panier</h4>
                            <?php
                            for ($i = 0; $i < count($panier->annonces); $i++) {
                                $sous_total += $panier->annonces[$i]->prix;
                            ?><div style="border: 1px solid #F19820;padding:10px;margin:10px">
                                    <h2><?= $panier->annonces[$i]->titre ?></h2>
                                    <p><?= $panier->annonces[$i]->description ?></p>
                                    <p><?= $panier->annonces[$i]->prix ?> €</p>
                                </div><?php }
                                    $montant_total = $sous_total - $reductions + $frais_livraison;
                                        ?>
                            <!-- Recapitulatif -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="alert alert-primary">
                            <h4>Récapitulatif de votre commande</h4>
                            Sous-total de vos achat : <?= number_format($sous_total, 2) ?>€<br>
                            Reductions : <?= number_format($reductions, 2) ?>€<br>
                            Livraison : <?= number_format($frais_livraison, 2) ?>€<br>
                            <p>Montant Total <?= number_format($montant_total, 2) ?>€</p>
                        </div>
                    </div>
                </div>
                <!-- Fin recapitulatif -->
            </div>
        </div>
        <!-- Fin panier -->
    </div>
    </div>
    <?php
    $panier->cloturerPanier();
    ?>
</body>

</html>