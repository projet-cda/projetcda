<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
$user = new User();
$user->loadUser($_SESSION['ID']);
$value = 0;
if (!empty($_GET['statut'])) {
    $value = intval($_GET['statut']);
}
$user->getAnnonces($value);
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tableau de bord</title>
</head>

<body>
    <?php
    require('header.php');
    if ($user->isBanned) {
    ?>
        <h2>Compte désactivé, veuillez contacter l'administrateur du site</h2>
    <?php
    } else {
    ?>
        <div class="container" style="margin: 100px auto;">
            <h1 class="display-4">Tableau de bord "<?=$user->nom?> <?=$user->prenom?>"</h1>
            <div class="jumbotron" style="background-color:#F19820">
                <ul class="nav nav-pills nav-fill nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="panel.php">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="panel_stat.php">Mes stats</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="panel_archive.php">Mes commandes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="panel_infos.php">Mes infos</a>
                    </li>
                </ul>
                <h1>Mes statistiques</h1>
                <div class="alert alert-primary">
                    <p>Nombre d'annonces en cours : <?= $user->infos['en_cours'] ?></p>
                    <p>Nombre d'annonces terminées dans le mois : <?= $user->infos['terminee'] ?></p>
                    <p>CA réalisé : <?= $user->infos['CA'] ?>€</p>
                </div>
            </div><?php } ?>
        </div>
</body>

</html>