<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}


// Ajout de l'annonce au panier
if (isset($_GET['idAnnonce'])) {
    $idAnnonce = intval($_GET['idAnnonce']);
    $panier = new Panier();
    $panier->verifExistPanier();
    $panier->ajoutPanier($idAnnonce);
}

header("Location: detail.php?idAnnonce=$idAnnonce");
die;
