<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");

// Si l'utlisateur n'est pas loggé renvoie vers l'index
if (!isset($_SESSION['ID'])) {
    header("Location:index.php");
    die;
}

// Suppression de l'annonce
if (isset($_GET['idAnnonce'])) {
    $uneAnnonce = Annonce::construct_load($_GET['idAnnonce']);
    $uneAnnonce->photo->deletePhoto();
    $uneAnnonce->deleteAnnonce();
}

if (isset($_GET['isAdmin'])) {
    header("Location: admin.php?modif=ok");
    die;
}

header("Location: panel.php?modif=ok");
die;
