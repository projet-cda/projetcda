<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si lu'utlisateur est déjà loggé, renvoie vers l'index
if (isset($_SESSION['ID'])) {
    header("Location:index.php");
}
// Traitement du post
$erreur = null;
$user = new User();
if (!empty($_POST)) {
    $email = null;
    if (!empty($_POST['email'])) {
        $user->email = addslashes(htmlspecialchars(trim(stripslashes(strip_tags(($_POST['email']))))));
    }
    $pwd = null;
    if (!empty($_POST['pwd'])) {
        $user->pwd = $_POST['pwd'];
    }
    if ($user->existUser()) {
        $user->loadUser($user->id);
        $_SESSION['ID'] = $user->id;
        $_SESSION['isAdmin'] = $user->isAdmin;
        if ($_SESSION['isAdmin']) {
            header("Location:admin.php");
        } else {
            header("Location:panel.php");
        }
    } else {
        $erreur = "Erreur d'identification";
    }
}
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <script src="lib/js/jquery-3.5.1.min.js"></script>
    <script src="lib/js/jquery.validate.js" referrerpolicy="origin"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Identification</title>
</head>

<body>
    <?php
    require('header.php');
    ?>
    <br>
    <div class="container">


        <div class="container jumbotron" style="width: 700px; background-color:#F19820">


            <h1 class="display-4">Identification</h1><br>
            <?php
            if ($erreur) {
                echo "<div class='alert alert-danger'>$erreur</div>";
            }
            ?>
            <form action="#" method="POST">
                <div class="form-group">
                    <label for="email">Email : </label>
                    <input class="form-control" width="100px" type="email" name="email" placeholder="email" required><br>
                    <label for="pwd">Mot de passe : </label>
                    <input class="form-control" type="password" name="pwd" required><br>
                    <input class="btn btn-primary" type="submit" value="Se connecter">
                </div>
            </form>

            <a href="register.php">S'inscrire</a>
        </div>
    </div>
    </div>
</body>

</html>