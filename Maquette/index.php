<?php
session_start();

// 
function tronquer_texte($texte, $nbchar)
{
    return (strlen($texte) > $nbchar ? substr(
        substr($texte, 0, $nbchar),
        0,
        strrpos(substr($texte, 0, $nbchar), " ")
    ) . " (...)" : $texte);
}

// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Recupération des annnonces
$site = new Model();
$site->getAnnoncesEnCours();
if (isset($_SESSION['ID'])) {
    if ($_SESSION['isAdmin']) {
        header("Location:admin.php");
    } else {
        $panier = new Panier();
        $panier->verifExistPanier();
    }
}
// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <link rel="stylesheet" href="lib/css/styles3.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accueil</title>
</head>

<body>
    <?php
    require('header.php');
    ?>
    <div class="container" style="margin: 100px auto;">
        <div class="jumbotron" style="background-color:#F19820">
            <h1 class="display-4">Liste des annonces</h1><br>
            <?php
            for ($i = 0; $i < count($site->annonces); $i++) { ?>
                <div class="card float-left bg-light" style="width: 18rem; margin:30px;">
                    <?php if (isset($site->annonces[$i]->photo->id)) { ?>
                        <img src="<?= $site->annonces[$i]->photo->url ?>" class="card-img-top" alt="Image annonce <?= $site->annonces[$i]->id ?>">
                    <?php } else { ?>
                        <img src="img/defaut.jpg" class="card-img-top" alt="image par defaut">
                    <?php } ?>
                    <div class="card-body">
                        <h5 class="card-title"><?= $site->annonces[$i]->titre ?></h5>
                        <p>Publiée le <?= $site->annonces[$i]->date->format("d/m/Y à H:i") ?></p>
                        <div style="height: 100px;" class="card-text"><?= tronquer_texte($site->annonces[$i]->description, 80) ?></div>
                        Prix : <?= $site->annonces[$i]->prix ?> €<br>
                        <a href="detail.php?idAnnonce=<?= $site->annonces[$i]->id ?>" class="btn btn-primary">Plus d'info</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</body>

</html>