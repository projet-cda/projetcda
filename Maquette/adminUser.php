<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");

// Si l'utlisateur n'est pas loggé ou pas admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || (!$_SESSION['isAdmin'])) {
    header("Location:index.php");
    die;
}
$user = new User();
$user->loadUser($_SESSION['ID']);

$param = null;
if (!empty($_GET['param'])) {
    $param = intval($_GET['param']);
}
$site = new Model();
$site->getAllAnnonces($param);
$site->getAllUsers();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panneau Admin</title>
</head>

<body>
    <?php
    require('header.php');
    ?>
    <div class="container" style="margin: 100px auto;">
        <h1 class="display-4">Tableau de bord Administrateur</h1><br>
        <div class="jumbotron" style="background-color:#F19820">
            <ul class="nav nav-pills nav-fill nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="admin.php">En cours</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?param=2">En attente</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?param=3">Terminées</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="adminUser.php">Utilisateurs</a>
                </li>
            </ul>
            <div class="container" style="margin-top: 50px;">
                <?php if (!empty($_GET['modif'])) {
                    echo "<div class='alert alert-danger'>Mise à jour effectuée</div>";
                } ?>
                <table class="table table-hover table-primary">
                    <thead class="thead-dark">
                        <tr>
                            <th>Utilisateur</th>
                            <th>Bannir</th>
                            <th>Statut</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for ($i = 0; $i < count($site->users); $i++) { ?>
                            <tr>
                                <td><?= $site->users[$i]->nom ?> <?= $site->users[$i]->prenom ?></td>
                                <?php if ($site->users[$i]->isBanned) { ?>
                                    <td><button class="btn btn-danger" onClick="Javascript:if (confirm('Débannir ?')){window.location='ban.php?idUser=<?= $site->users[$i]->id ?>&isAdmin=1&ban=0'}">Débannir</button></td>
                                <?php } else { ?>
                                    <td><button class="btn btn-danger" onClick="Javascript:if (confirm('Bannir ?')){window.location='ban.php?idUser=<?= $site->users[$i]->id ?>&isAdmin=1&ban=1'}">Bannir</button></td>
                                <?php } ?>
                                <td>
                                    <?php
                                    if ($site->users[$i]->isBanned == 1) {
                                        echo "Banni";
                                    } else {
                                        echo "Non-banni";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>