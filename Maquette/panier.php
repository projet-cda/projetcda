<?php
session_start();
// Autoloadeur
function chargeur($cl)
{
    include("lib/classes/" . $cl . ".php");
}
spl_autoload_register("chargeur");
// Si l'utlisateur n'est pas loggé ou c'est un admin, renvoie vers l'index
if (!isset($_SESSION['ID']) || ($_SESSION['isAdmin'])) {
    header("Location:index.php");
}
$panier = new Panier();
$panier->idClient = $_SESSION['ID'];
$panier->getAnnonces();
// Initialisation des variables pour le recapitulatif
$sous_total = 0;
$reductions = 0;
$frais_livraison = 6.95;
$montant_total = 0;

// Rendu
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="lib/css/styles.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
</head>

<body>
    <?php
    require('header.php');
    ?><br>
    <div class="container" style="margin: 100px auto;">
        <div class="jumbotron" style="background-color:#F19820">
            <ul class="nav nav-pills nav-fill nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Panier</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Livraison</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Paiement</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">Confirmation</a>
                </li>
            </ul>

            <!-- Panier -->
            <div class="container">
                <h1>Panier</h1>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="alert alert-warning">
                            <?php

                            if (count($panier->annonces)) {
                                for ($i = 0; $i < count($panier->annonces); $i++) {
                                    $sous_total += $panier->annonces[$i]->prix;
                            ?><div style="border: 1px solid #F19820;padding:10px;margin:10px">
                                        <h2><?= $panier->annonces[$i]->titre ?></h2>
                                        <p><?= $panier->annonces[$i]->description ?></p>
                                        <p><?= $panier->annonces[$i]->prix ?> €</p>
                                        <button style="margin-bottom: 10px;" class="btn-secondary" onclick="Javascript:if (confirm('Supprimer ?')){window.location='deleteFromPanier.php?idAnnonce=<?= $panier->annonces[$i]->id ?>&idPanier=<?= $panier->id ?>'}">Supprimer du panier</button>
                                    </div><?php }
                                        $montant_total = $sous_total - $reductions + $frais_livraison;
                                            ?>
                        </div>
                        <form action="livraison.php">
                            <input class="btn-success" type="submit" value="Valider le panier">
                        </form>
                    </div>

                    <!-- Recapitulatif -->
                    <div class="col-sm-4">
                        <div class="alert alert-primary">
                            <h4>Récapitulatif de votre commande</h4>
                            Sous-total de vos achat : <?= number_format($sous_total, 2) ?>€<br>
                            Reductions : <?= number_format($reductions, 2) ?>€<br>
                            Livraison : <?= number_format($frais_livraison, 2) ?>€<br>
                            <p>Montant Total <?= number_format($montant_total, 2) ?>€</p>
                        </div>
                    </div>
                    <!-- Fin recapitulatif -->
                <?php } else {
                                echo "Panier vide";
                            } ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>